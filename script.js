// State of the Todos 
let tasksState = []
let id = -1
let toggleState = true

// Get DOM elements
const input = document.getElementById("new-task-input")
const addTask = document.getElementById("add-task-button")
const allTasksDiv = document.getElementById("task-details-list")
const activeTasksDiv = document.getElementById("active-tasks-list")
const completedTasksDiv = document.getElementById("completed-tasks-list")
const rightHalfDiv = document.getElementById("right-half")
const toggleCheckboxs = document.getElementById("toggle-checkboxs")
const removeCompletedTasks = document.getElementById("remove-items-button")
const tasksCounterDiv = document.getElementById("tasks-counter")



// Event listeners
addTask.onclick = () => addTaskInTheState(input)

input.addEventListener('keydown',  (event) => {
    if (event.key === 'Enter') {
        event.preventDefault()
        addTaskInTheState(input)
    }
})


input.addEventListener('input' , () => {
    input.style.backgroundColor = 'white';
})

rightHalfDiv.addEventListener('click', (event) => {
    if (event.target.tagName === 'BUTTON') {
        const id = event.target.parentElement.id
        removeElement(id)
    } else if (event.target.tagName === 'INPUT') {
        const id = event.target.parentElement.id
        ToggleCheckInput(id)
    } 
})


rightHalfDiv.addEventListener('dblclick', (event) => {
    if (event.target.tagName === 'SPAN') {
        event.target.contentEditable = true
        event.target.focus()

        event.target.addEventListener('input', () => {
            setTimeout(() => {
                updateSpanText(event.target.parentElement.id, event.target.innerText)
            }, 3500)
        })
    }

})


toggleCheckboxs.onclick = () => toggleAll()

removeCompletedTasks.onclick = () => clearCompletedTasks()




// Functions

const addTaskInTheState = (input) => {

    if (input.value.length >= 2) {
        id = id + 1
        tasksState.push({'id': id, 'value': input.value, 'checked': false })

        input.value = ''

        updateUI()
    }
    
}

const updateDiv = (divContainer, tasksState) => {

    divContainer.innerHTML = ''

    for (const task of tasksState) {
        
        const li = document.createElement('li')
        const fragment = document.createDocumentFragment()

        const inputElement = document.createElement('input')
        const buttonElement = document.createElement('button')
        const spanElement = document.createElement('span')

        inputElement.type = 'checkbox'
        buttonElement.textContent = 'x'

        spanElement.textContent = task.value
        inputElement.checked = task.checked

        fragment.append(inputElement)
        fragment.append(spanElement)
        fragment.append(buttonElement)

        li.id = task.id
        li.append(fragment)

        divContainer.append(li)

    }
}



const stringToNum = (id) => {
    return parseInt(id, 10)
}
   

const removeElement = (id) => {
    const numericId = stringToNum(id)
    tasksState = tasksState.filter(task => task.id !== numericId)
    updateUI()
}



const ToggleCheckInput = (id) => {
    const numericId = stringToNum(id)
    const taskToUpdate = tasksState.find(task => task.id === numericId)

    if (taskToUpdate) {
        taskToUpdate.checked = !taskToUpdate.checked
        updateUI();
    }
}



const toggleAll = () => {
    toggleState = !toggleState
    tasksState.forEach((task) => task.checked = toggleState)
    updateUI()
}



const clearCompletedTasks = () => {
  tasksState = tasksState.filter((task) => task.checked === false)
  updateUI()
}



const countTasks = () => {
    let tasksCount = tasksState.filter((task) => task.checked === false).length
    tasksCounterDiv.innerText = tasksCount === 1 ? `1 item left!` : `${tasksCount} items left`
}

const updateSpanText = (id, value) => {
    console.log('id', id, 'value', value)
    const newTaskValue = value.trim()
    const numericId = stringToNum(id)

    let task = tasksState.find(task => task.id == numericId)
    
    if (task) {
        task.value = newTaskValue
        updateUI()
    }
}

const updateUI = () => {
    updateDiv( allTasksDiv, tasksState ) 
    updateDiv( completedTasksDiv, tasksState.filter((task) => task.checked) ) 
    updateDiv( activeTasksDiv, tasksState.filter((task) => task.checked === false) ) 
    countTasks()
}

